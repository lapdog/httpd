/*
   Copyright 2014 The Lapdog Team

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

package main

import (
	"flag"
	"html/template"
	"net/http"
	"path/filepath"

	"github.com/golang/glog"
)

var (
	TemplatesDir    = flag.String("templates-dir", "templates", "path to directory containing HTML templates")
	DocumentRoot    = flag.String("document-root", "www", "path to HTTP document root (for static files)")
	ListenAddr      = flag.String("listen-addr", "127.0.0.1:5000", "address:port to listen on")
	GroupcachePeers = flag.String("groupcache-peers", "", "comma-separated list of groupcache peers")
	Templates       *template.Template
)

func init() {
	flag.Parse()
}

func main() {
	var err error
	// Load the templates.
	Templates, err = template.ParseGlob(filepath.Join(*TemplatesDir, "*.tmpl.html"))
	if err != nil {
		glog.Fatalln(err)
	}

	http.Handle("/static/", http.FileServer(http.Dir(*DocumentRoot)))
	http.HandleFunc("/sign-in", SignInHandler)
	http.HandleFunc("/", IndexHandler)
	glog.Infof("Listening on %s", *ListenAddr)
	if err := http.ListenAndServe(*ListenAddr, nil); err != nil {
		glog.Fatalln(err)
	}
	glog.Infof("Shutting down")
}

func IndexHandler(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case "GET":
		err := Templates.ExecuteTemplate(w, "index", "")
		if err != nil {
			glog.Errorf("Failed to execute template %q: %v", "index", err)
			http.Error(w, err.Error(), http.StatusInternalServerError)
		}

	default:
		http.Error(w, "Method not supported", http.StatusMethodNotAllowed)
	}
}

func SignInHandler(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case "GET":
		if err := Templates.ExecuteTemplate(w, "signin", ""); err != nil {
			glog.Errorf("Failed to execute template %q: %v", "signing", err)
			http.Error(w, err.Error(), http.StatusInternalServerError)
		}

	case "POST":
		username := r.FormValue("username")
		password := r.FormValue("password")
		if username == "" || password == "" {
			http.Error(w, "Empty username or password", http.StatusBadRequest)
			return
		}
		http.Error(w, "Not fully implemented", http.StatusNotImplemented)

	default:
		http.Error(w, "Method not supported", http.StatusMethodNotAllowed)
	}
}
