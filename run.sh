#!/usr/bin/env bash

cleanup() {
    rm -f httpd
}

trap cleanup EXIT

go build -o httpd .
if [[ "$?" == "0" ]]; then
    ./httpd -logtostderr -v=2 \
	-listen-addr="127.0.0.1:5000" \
	-document-root="www" \
	-templates-dir="templates"
fi
