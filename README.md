# lapdog/http

The web server that hosts the Lapdog web interface.

## License

All of the code in this project is kept under the Apache License, version 2.0.
