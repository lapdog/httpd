/*
   Copyright 2014 The Lapdog Team

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

package main

import (
	"bytes"
	"crypto/rand"
	"crypto/sha512"
	"fmt"
	"strconv"
	"strings"
	"time"

	"code.google.com/p/go.crypto/pbkdf2"
)

const (
	SaltLength           = 12
	NPasswordInterations = 4096
	PasswordLength       = 48
)

type (
	User struct {
		ID          int
		Email       string
		Password    string
		LastUpdated time.Time
		Created     time.Time
	}
)

func (u *User) SetPassword(p string) error {
	salt := make([]byte, SaltLength)
	if _, err := rand.Read(salt); err != nil {
		return err
	}
	pw, err := u.hashPassword([]byte(p), nil, NPasswordInterations, PasswordLength)
	if err != nil {
		return err
	}
	u.Password = fmt.Sprintf("sha512|%s|%d|%s", string(salt), NPasswordInterations, string(pw))
	return nil
}

func (u *User) hashPassword(p, salt []byte, iter, length int) ([]byte, error) {
	pw := pbkdf2.Key(p, salt, iter, length, sha512.New)
	return pw, nil
}

func (u *User) CheckPassword(p string) (bool, error) {
	// Parse the current password to get the hashing details.
	parts := strings.SplitN(u.Password, "|", 4)
	cpw := parts[3]
	csalt := parts[1]
	niter, err := strconv.Atoi(parts[2])
	if err != nil {
		return false, err
	}

	var npw []byte
	npw, err = u.hashPassword([]byte(p), []byte(csalt), niter, PasswordLength)
	if err != nil {
		return false, err
	}

	return bytes.Equal([]byte(cpw), npw), nil
}
