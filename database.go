/*
   Copyright 2014 The Lapdog Team

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

package main

import (
	"database/sql"
	"os"

	"github.com/golang/glog"
	_ "github.com/lib/pq"
)

var DB *sql.DB

func init() {
	if dburl := os.Getenv("DATABASE_URL"); dburl == "" {
		glog.Fatalln("DATABASE_URL not specified")
	} else {
		glog.V(1).Infoln("Connecting to database")
		var err error
		DB, err = sql.Open("postgres", dburl)
		if err != nil {
			glog.Fatalln("Could not connect to database:", err)
		}
	}

	if err := DB.Ping(); err != nil {
		glog.Fatalln("Error pinging database server:", err)
	}
}

func WithDB(fn func(tx *sql.Tx) error) error {
	transaction, err := DB.Begin()
	if err != nil {
		return err
	}
	if err = fn(transaction); err != nil {
		return err
	}
	if err = transaction.Commit(); err != nil {
		return transaction.Rollback()
	}
	return nil
}
